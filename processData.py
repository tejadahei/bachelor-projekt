import numpy as np

from os import path
from pathlib import Path
import sys


## hier wird die Wandschubspannung bei der Winkelvariation auf den tau bei alpha=0 normiert

utaus=[1.13]
uTauAngle={}

## alle geschwindigkeiten laden als dict

for i in utaus:
	uTauAngle[i]=np.loadtxt("Data/angleVariation_tauw_"+str(i)+".csv", skiprows=1, delimiter=',')
print(uTauAngle)



for i in uTauAngle.keys():
	b=uTauAngle[i]
	normValue=b[b[:,0]==0,:][0,1]
	b[:,1]=(b[:,1]-normValue)/normValue
	np.savetxt(path.join("Data","normedAngleVariation_utau_"+str(i)+".csv"), b, delimiter=", ", header="alpha, utaunorm",comments='')
	print(b)

## alle geschwindigkeiten laden als dict

for i in utaus:
	uTauAngle[i]=np.loadtxt("Data/angleVariation_tauw_"+str(i)+".csv", skiprows=1, delimiter=',')
print(uTauAngle)



for i in uTauAngle.keys():
	b=uTauAngle[i]
	normValue=b[b[:,0]==0,:][0,1]
	b[:,1]=(b[:,1]-normValue)/normValue
	np.savetxt(path.join("Data","normedAngleVariation_tauw_"+str(i)+".csv"), b, delimiter=", ", header="alpha, tauwnorm",comments='')
	print(b)
