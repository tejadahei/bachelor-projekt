#!/usr/bin/python 
import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time
from pythonCaseLibs import *
import threading
from os import path
from pathlib import Path
import math

import tikzplotlib
import sys
utaucol='red'
dTcol='blue'
allCases={}
ut={}
tw={}
for j in sys.argv[1:]:
	dirChild=j
	print(j)
	directory=Path('angleVariation').iterdir()
	cases={}
	for i in directory:
		if str(i)!="angleVariation/template" and i.is_dir():
			try:
				j=path.join(i,"u_tau_"+dirChild)
				#print(os.path.basename(i))
				cases[float(os.path.basename(i))]=toolbox.parseTemp(str(j))[-1]
				
				
			except:
				pass
	result=cases.items()
	data=np.array(list(result))
	allCases[j]=data
	data_utau=np.copy(data)
	data_utau[:,1]=0.034*data_utau[:,1]
	data_tauw=np.copy(data_utau)
	data_tauw[:,1]=data_tauw[:,1]**2*1.225
	ut[j]=data_utau
	tw[j]=data_tauw
	np.savetxt(path.join("Data","angleVariation_"+dirChild+".csv"), data, delimiter=", ", header="alpha, dT",comments='')
	np.savetxt(path.join("Data","angleVariation_utau_"+dirChild+".csv"), data_utau, delimiter=", ", header="alpha, utau",comments='')
	np.savetxt(path.join("Data","angleVariation_tauw_"+dirChild+".csv"), data_tauw, delimiter=", ", header="alpha, tauw",comments='')
fig,ax = plt.subplots(1,1)
for i in allCases.keys():
	data_tauw=tw[i]
	ax.scatter(data_tauw[:,0],data_tauw[:,1])
#	ax.scatter(data_tauw[:,0],data_tauw[:,1])
	vglData=np.linspace(0,180,100)
	ax.plot(vglData,0.5*max(data_tauw[:,1])*(np.cos(math.pi*vglData/90))+0.5*max(data_tauw[:,1]),'--','lightgrey')
	
	
ax.set_title(r'$Zusammenhang\ zwischen\ \Delta T\ und\ \alpha$')
ax.set_xlabel(r'$\alpha [^\circ]$')
ax.set_ylabel(r'$\Delta T\ [K]$', color=dTcol)
ax.legend()
ax.grid()
plt.yscale('linear')
tikzplotlib.save('./Bilder/plotAngleVariation.tex')
plt.savefig('./Bilder/plotAngleVariation.png', dpi=300)
plt.show()


