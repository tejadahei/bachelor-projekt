#!/usr/bin/python 
import numpy as np
import matplotlib.pyplot as plt
import os
import time as Time
from pythonCaseLibs import *
import threading
from os import path
from pathlib import Path
import math
from scipy.optimize import curve_fit
#import tikzplotlib
import sys


dirChild=sys.argv[1]
directory=Path(path.join('uTauVariation',dirChild)).iterdir()
utauEx,dTEx = np.load('./sensorData.npy') 
cases={}
for i in directory:
    if str(i)!="uTauVariation/template" and i.is_dir():
        print(i)
        ut=interpolation.interpolateOverHeater(0,i,False)
        cases[ut]=toolbox.parseTemp(str(i))[-1]
        print("plotCurve says u_tau_Einlass:" +str(i)+"\n u_tau: "+str(ut))

result=cases.items()
data=np.array(list(result))

print(data[:,0])
file=open("Data/linearModel_"+str(dirChild)+".txt","w")
def f(x,a1,a0): 
    return a1*x+a0
popt, pcov = curve_fit(f, data[:,1], data[:,0])
print(str(popt))
file.write("linear Model y=mx+b\na:    "+str(popt[0])+"\n  b:  "+str(popt[1])+"\n Koeffizienten:   "+str(pcov)+"\n Standardabweichung: "+str(np.sqrt(np.diag(pcov))))
file.close()
np.savetxt("Data/uTauVariation_"+str(dirChild)+".csv", data, delimiter=", ", header='utau, dT',comments='')

dataTau=np.copy(data)

dataTau[:,0]=dataTau[:,0]**2*1.225

np.savetxt("Data/uTauVariation_tauw_"+str(dirChild)+".csv", dataTau, delimiter=", ", header='tauw, dT',comments='')
plt.scatter(data[:,0],data[:,1])
plt.savefig('pltvariation.png')
