#!/usr/bin/python 
import numpy as np
#import matplotlib.pyplot as plt
import os
import time as Time
from pythonCaseLibs import *
import threading
from os import path
from pathlib import Path
import math

#import tikzplotlib
import sys
utaucol='red'
dTcol='blue'
dirChild=sys.argv[1]
directory=Path('angleVariation').iterdir()
cases={}
for i in directory:
	if str(i)!="angleVariation/template" and i.is_dir():
		try:
			j=path.join(i,"u_tau_"+dirChild)
			print(os.path.basename(i))
			cases[float(os.path.basename(i))]=toolbox.parseTemp(str(j))[-1]

		except:
			pass

result=cases.items()
data=np.array(list(result))

utau=interpolation.interpolateOverHeater(0,path.join("angleVariation/0.0", "u_tau_"+dirChild))
np.savetxt("Data/angleVariation_uTau_"+str(round(utau, 2))+".csv", data[data[:,0].argsort()], delimiter=", ",header="alpha, dT", comments="")


m=0.032
b=0.105

dataTau=np.copy(data)
dataTau[:,1]=(dataTau[:,1]*m+b)**2*1.225
np.savetxt("Data/angleVariation_tauw_"+str(round(utau**2*1.225, 2))+".csv",  dataTau[dataTau[:,0].argsort()], delimiter=", ",header="alpha, tauw", comments="")

dataTauNorm=np.copy(dataTau)
dataTauNorm[:,1]=(dataTauNorm[:,1]-max(dataTauNorm[:,1]))/max(dataTauNorm[:,1])
np.savetxt("Data/normedAngleVariation_tauw_"+str(round(utau**2*1.225, 2))+".csv",  dataTauNorm[dataTauNorm[:,0].argsort()], delimiter=", ",header="alpha, tauwnorm", comments="")

#fig,ax = plt.subplots(1,1)
#ax.scatter(data[:,0],data[:,1])
#vglData=np.linspace(0,180,100)
#ax.plot(vglData,max(data[:,1])*np.cos(math.pi*vglData/180),'--','lightgrey')
#ax.set_title(r'$Zusammenhang\ zwischen\ \Delta T\ und\ \alpha$')
#ax.set_xlabel(r'$\alpha [^\circ]$')
#ax.set_ylabel(r'$\Delta T\ [K]$', color=dTcol)

#ax.legend() 
#ax.grid()
#plt.yscale('linear')
#tikzplotlib.save('./Bilder/plotAngleVariation.tex')
#plt.savefig('./Bilder/plotAngleVariation.png', dpi=300)
#plt.show()


