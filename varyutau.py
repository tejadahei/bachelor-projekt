#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
import math
import os
from os import path
with open('params.json', 'r') as fp:
    paramDict = json.load(fp)
utaumin=0.2
utaumax=1.4
utauArray=np.linspace(utaumin,utaumax, 5)
#utauArray=[1.0]
utauAngles=[0]
print(utauArray)
for i in utauAngles:
	os.system('rm -r '+path.join("uTauVariation",str(i)))
	os.system('mkdir '+path.join("uTauVariation",str(i)))
	analyseCase.utauVariation(utauArray,i,path.join("uTauVariation",str(i)),True,1e-4,N=8)
