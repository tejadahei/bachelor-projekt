#!/bin/python
import sys
from os import path
import os
from pythonCaseLibs import grenzschichtprofil
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import math

thermophysicalProperties = ParsedParameterFile(path.join("template","constant", "thermophysicalProperties"),backup = True )

As=thermophysicalProperties["mixture"]["transport"]["As"]
Ts=thermophysicalProperties["mixture"]["transport"]["Ts"]
T=293.00
Rm=8.3144621
M=thermophysicalProperties["mixture"]["specie"]["molWeight"]*1e-3
R=Rm/M
rho=1e5/(R*T)
mu=As*math.sqrt(T)/(1+Ts/T)
nu=mu/rho
ut=sys.argv[1]
d=sys.argv[2]
grenzschichtprofil.genRealVelocityProfile(float(nu),float(ut),2e-3,200,float(d))
print(str(rho))
