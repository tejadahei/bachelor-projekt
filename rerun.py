#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
import os
import sys
from pathlib import Path


listOfDirs=sys.argv[1:-1]
analyseCase.rerun(listOfDirs,True,5e-5,N=8)
