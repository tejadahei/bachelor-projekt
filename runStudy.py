#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
with open('params.json', 'r') as fp:
    paramDict = json.load(fp)
widthArray=np.array([190])
analyseCase.consecutiveRun(widthArray,paramDict,True,1e-4,N=8)
 
