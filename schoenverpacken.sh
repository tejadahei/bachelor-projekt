#!/bin/bash
curDir=$(pwd)
rm -rf $curDir/gitterstudie
mkdir $curDir/gitterstudie
	mkdir $curDir/gitterstudie/$1
	cp -r $curDir/$1/constant $curDir/gitterstudie/$1
	cp -r $curDir/$1/system $curDir/gitterstudie/$1
	cp -r $curDir/$1/0 $curDir/gitterstudie/$1
	reconstructPar -case $curDir/$1 -latestTime
	toCopy=$(ls -d $curDir/$1[0-9]* | sort -nr | head -n1)
	echo $toCopy
	cp -r $toCopy $curDir/gitterstudie/$1
	cp -r $curDir/$1/postProcessing $curDir/gitterstudie/$1
	cp -r $curDir/pythonCaseLibs $curDir/gitterstudie/
	cp -r $curDir/*.py $curDir/gitterstudie/
#	cp -r $curDir/Bilder $curDir/gitterstudie/
	cp -r $curDir/*.sh $curDir/gitterstudie/
tar -zcvf $curDir/gitterstudie.tar.gz $curDir/gitterstudie
rm -rf $curDir/gitterstudie
