# ! / usr / bin / env python3
import sys
from os import path
import os
import math
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Applications.PrepareCase import PrepareCase
from PyFoam.Applications.CloneCase import CloneCase
from PyFoam.Applications.Decomposer import Decomposer
from PyFoam.Applications.Runner import Runner
from . import toolbox,grenzschichtprofil
	
	
def genCase(params, n, alpha, utau):   
	""" 
		Hauptsächlich für die Gitterstudie: Gitter mit einer bestimmten Zellenanzahl 
		entlang einer Kavitätseite erstellen und Geometrie aus einer Dictionary ("Params")
		erstellen. Format von params:
		{
			"d":                     Abstand der Drähte zueinander,
			"direction":             Richtung der Strömung,
			"h_c":                   Kavitättiefe,
			"t":                     Drahtdicke,
			"theta":                 Verjüngung,
			"w_c":                   Kavitätweite,
			"w_h":                   Breite Heizdraht,
			"w_d":                   Breite Messdraht,
			"utau":		             Schubspannungsgeschwindigkeit,
			"ERz":		             Expansionsverhältnis z-Richtung,
			"ERzCavity":	         Expansionsverhältnis z-Richtung in der Kavität,
			"ERxy":			         Expansionsverhältnis in radialer Richtung,
		}
		"""  
	caseName = 'case_n_'+str(n)
	print("Create case "+caseName)
	
	os.system('rm -r '+caseName)
	CloneCase(args=["template",caseName])	
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = False )
	U = ParsedParameterFile(path.join(caseName,"0","U"),backup = False )
	domainDimensions["ERxy"]=params["ERxy"]
	domainDimensions["ERz"]=params["ERz"]
	domainDimensions["nCavity"]=int(n)
	domainDimensions["ERzCavity"]=params["ERzCavity"]
	domainDimensions["w_c"]=params["w_c"]
	domainDimensions["w_d"]=params["w_d"]
	domainDimensions["t"]=params["t"]
	domainDimensions["w_h"]=params["w_h"]
	domainDimensions["w_c_orig"]=params["w_c"]
	domainDimensions["d"]=params["d"]
	domainDimensions["h_c"]=params["h_c"]
	domainDimensions["theta"]=params["theta"]
	domainDimensions["uTau"]=utau
	domainDimensions.writeFile()
	domainDimensions["directionRad"]=math.pi*alpha/180

	domainDimensions.writeFile()
	U.writeFile()
	grenzschichtprofil.genRealVelocityProfile(toolbox.calcNu(caseName),utau,domainDimensions["hoehe"],300,0.0,caseName)
	return caseName

def bakeMesh(caseName,parallel=False,np=4):
	print("bakeMesh")
	grenzschichtprofil.genSampleLines(20,caseName)
	Runner(["blockMesh", "-case",caseName])
	Runner(["snappyHexMesh","-overwrite", "-case",caseName])
	
	Runner(["topoSet", "-case",caseName])
	Runner(["createPatch","-overwrite", "-case",caseName])
	
	if parallel:
		Decomposer([caseName,np])
		#Runner(["mapFields", "-case",caseName, 'case_dxy_2e-05','-parallelSource','-parallelTarget'])


def varyUtauMesh(utau,variationDirectory,parallel=True,np=4,angle=0.0):
		caseName="u_tau_"+str(utau)
		
		os.system('rm -r '+path.join(variationDirectory,caseName))
		os.system('mkdir '+path.join(variationDirectory,caseName))
		
		os.system('rm -r '+path.join(variationDirectory,caseName))
		CloneCase(args=[path.join("uTauVariation","template"),path.join(variationDirectory,caseName)])
		
		domainDimensions = ParsedParameterFile(path.join(path.join(variationDirectory,caseName),"constant","domainDimensions"),backup = True)
		
		domainDimensions["directionRad"]=math.pi*angle/180
		domainDimensions.writeFile()
		
		grenzschichtprofil.genSampleLines(20,path.join(variationDirectory,caseName))
		grenzschichtprofil.genRealVelocityProfile(toolbox.calcNu(path.join(variationDirectory,caseName)),utau,domainDimensions["hoehe"],300,angle,path.join(variationDirectory,caseName))
		
		Runner(["topoSet", "-case",path.join(variationDirectory,caseName)])
		Runner(["createPatch","-overwrite", "-case",path.join(variationDirectory,caseName)])
		if parallel:
			Decomposer([path.join(variationDirectory,caseName),np])
	
		return caseName
