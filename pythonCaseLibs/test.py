# ! / usr / bin / env python3
import sys
from os import path
import math
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Applications.PrepareCase import PrepareCase
from PyFoam.Applications.CloneCase import CloneCase
from PyFoam.Applications.Runner import Runner
import toolbox
params={"d":5e-6,
	"direction":0.0,
	"dz":	1e-6,
	"h_c":	200e-6,
	"w_c":	1000e-6,
	"w_h":	25e-6,
	"theta": 54.7,
	"t": 0.8e-6,
	"yPlus":10
	}
if len (sys.argv) != 3:
    print ( "gültiges dxy , U angeben!" )
    print(len(sys.argv))
    sys.exit (1)

dxy=float(sys.argv[1])
UBetrag=float(sys.argv[2])
caseName = 'case_dxy_'+str(dxy)
print(caseName)
CloneCase(args=["template",caseName])
domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )


domainDimensions["dxy"]=dxy
domainDimensions["w_c"]=params["w_c"]
domainDimensions["t"]=params["t"]
domainDimensions["w_h"]=params["w_h"]
domainDimensions["w_c_orig"]=params["w_c"]
domainDimensions["d"]=params["d"]
domainDimensions["h_c"]=params["h_c"]
domainDimensions["theta"]=params["theta"]
domainDimensions["UBetrag"]=UBetrag
domainDimensions["d_z"]=toolbox.calcyPlus(caseName,params["yPlus"])
domainDimensions["directionRad"]=math.pi*params["direction"]/180
domainDimensions["breite"]=params["w_c"]*domainDimensions["factorY"]
domainDimensions["laenge"]=params["w_c"]*domainDimensions["factorX"]
domainDimensions["hoehe"]=params["w_c"]*domainDimensions["factorZ"]
domainDimensions["dw_c_2"]=params["h_c"]/math.tan(math.pi*params["theta"]/180.0)
domainDimensions["hoehe"]=domainDimensions["laenge"]*math.sqrt(0.5)*0.5
domainDimensions["nh"]=abs(int(1-(math.log(1-domainDimensions["hoehe"]*(1-domainDimensions["ERz"])/params["dz"]*domainDimensions["ERz"])/math.log(domainDimensions["ERz"]))))

domainDimensions["nxyCavity"]=int(params["w_c"]/domainDimensions["dxy"])
					
domainDimensions["nzCavity"]=abs(int(1-(math.log(1-params["h_c"]*(1-domainDimensions["ERz"])/params["dz"]*domainDimensions["ERz"])/math.log(domainDimensions["ERz"]))));
domainDimensions["grading_Rechengebiet"]=domainDimensions["ERz"]**domainDimensions["nh"];
domainDimensions["nyRechenGebiet"]=abs(int(1-(math.log(1-0.5*(domainDimensions["laenge"]-params["w_c"])*(1-domainDimensions["ERxy"])/domainDimensions["dxy"]*domainDimensions["ERxy"])/math.log(domainDimensions["ERxy"]))));
domainDimensions["grading_Rechengebietx"]=domainDimensions["ERxy"]**domainDimensions["nxRechenGebiet"];


domainDimensions["circleThroughXY"]=math.sqrt(0.5)*domainDimensions["laenge"]*0.5
domainDimensions["nxRechenGebiet"]=abs(int(1-(math.log(1-0.5*(domainDimensions["breite"]-params["w_c"])*(1-domainDimensions["ERxy"])/domainDimensions["dxy"]*domainDimensions["ERxy"])/math.log(domainDimensions["ERxy"]))));
domainDimensions["grading_Rechengebiety"]=domainDimensions["ERxy"]**domainDimensions["nyRechenGebiet"];
domainDimensions["grading_Cavityz"]=domainDimensions["d_z"]/domainDimensions["dxy"];
#calc "$dz/$dxy"
domainDimensions.writeFile()






print(toolbox.calcReNr(caseName))
print(toolbox.calcyPlus(caseName,10))

