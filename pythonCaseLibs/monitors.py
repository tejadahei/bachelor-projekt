import time
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from . import toolbox

def convergenceMonitor(caseName,rtol):
	controlDict = ParsedParameterFile(path.join(caseName,"system","controlDict"),backup = True )
	controlDict["stopAt"]="endTime"
	controlDict.writeFile()
	converged=False
	while not converged:
		time.sleep(5)
		converged=toolbox.monitorConvergence2(caseName,tol=rtol)
	print('case converged!')
	controlDict["stopAt"]="writeNow"
	controlDict.writeFile()
