import math
import numpy as np
from os import path
import os
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import pickle
from . import plotResults
import warnings
from . import grenzschichtprofil
import os
from pathlib import Path

def calcNu (caseName):
    # Ursprünglich eine Funktion wegen langer unnötiger Rechnung, jetzt Verwendung von nu_0 aus der ISA
    nu = 14.607e-6
    return nu


def returnIterationNo(caseName):
    print(str(os.listdir('./'+caseName+'/processor0')))
    a=os.listdir('./'+caseName+'/processor0')
    b=[]
    for i in a:
        if i != 'constant':
            b.append(i)
    return int(max(np.array(b,dtype=int)))
	
def parseTemp(caseName):

        # Temperaturunterschied einlesen, falls Werte schon vorhanden sind

		TV=(np.genfromtxt('./'+caseName+'/postProcessing/tempVorne/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempVorne'),dtype=int)))+'/surfaceFieldValue.dat'))
		TH=(np.genfromtxt('./'+caseName+'/postProcessing/tempHinten/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/tempHinten'),dtype=int)))+'/surfaceFieldValue.dat'))
		
		try:
			TV[:,0]==TH[:,0]
			dT=TH[:,1]-TV[:,1]	
			T=TV[:,0]
		except Exception as e:
				print(e)
		return dT
		
def parseTau(caseName):

    # gemittelte Wandschubspannung bestimmen

	tau=(np.genfromtxt('./'+caseName+'/postProcessing/WSSAvg/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/WSSAvg'),dtype=int)))+'/surfaceFieldValue.dat'))
	return tau[:,1]
		
def parseResiduals(caseName):

    # Residuen einlesen
    
	res=(np.genfromtxt('./'+caseName+'/postProcessing/residuals/'+str(max(np.array(os.listdir('./'+caseName+'/postProcessing/residuals'),dtype=int)))+'/solverInfo.dat'))
	res_Ux=res[:,7]
	res_Uy=res[:,10]
	res_Uz=res[:,13]
	res_prgh=res[:,18]
	res_h=res[:,2]
	
	return res_Ux, res_Uy, res_Uz, res_prgh, res_h


def currentSlope(x,y):

    # Steigung einer Größe über 100 Einträge bestimmen

	x=x[len(x)-100:]
	y=y[len(y)-100:]
	n = np.size(x)
	# mean of x and y vector
	m_x = np.mean(x)
	m_y = np.mean(y) 
	# calculating cross-deviation and deviation about x
	SS_xy = np.sum(y*x) - n*m_y*m_x
	SS_xx = np.sum(x*x) - n*m_x*m_x
 
    # calculating regression coefficients
	b_1 = SS_xy / SS_xx
	b_0 = m_y - b_1*m_x
    
	return b_1, b_0

def isConverged(T,controlValue, trend):

    # Größe auf Konvergenz überprüfen

	if (len(controlValue)>100):
		slope, displacement=currentSlope(T,controlValue)
		print(slope)
		if (abs(slope)<trend):
			return True
		else:
			return False
	else:
		return False

def monitorConvergence2(caseName,log=open('convergenceLog','a'),tol=2e-3):

    # Wandschubspannung am Heizdraht und Temperaturunterschied auf Konvergenz überprüfen

	try:
		
		res_Ux, res_Uy, res_Uz, res_prgh, res_h=parseResiduals(caseName)
		dT=parseTemp(caseName)
		tau=parseTau(caseName)
	except Exception as e:
		log.write(str(e))
		return False
	else:
		convergenceCriteria=[isConverged(np.array(range(len(tau))),tau,tol),isConverged(np.array(range(len(dT))),dT,tol)]

		return all(convergenceCriteria)
		

def listDirectoriesWithPrefix(prefix):

    # Alle Ordner mit einem bestimmten Präfix auflisten

    current_directory = Path.cwd()
    directories_with_prefix = [entry.name for entry in current_directory.iterdir() if entry.is_dir() and entry.name.startswith(prefix)]
    return directories_with_prefix
