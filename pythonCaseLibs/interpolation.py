
import sys
import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.integrate
import csv
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from os import path
import os
from . import toolbox
import re
#import tikzplotlib
from scipy.optimize import curve_fit
from scipy.optimize import least_squares
from os.path import isfile, join
def importU(caseName):
    samplePath=path.join(caseName,"postProcessing","UsampleLines")
    dirToCheck=(max((np.array(os.listdir(samplePath),dtype=int))))
    w_c_2=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["w_c"]
    r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["radius"]
    onlyfiles = os.listdir(join(samplePath,str(dirToCheck)))
    arr={}
    foo=[]
    for i in onlyfiles:
        
        foo.append(float(i.split("_")[1]))
    maxnorm=max(abs(max(foo)), abs(min(foo)))
    for i in onlyfiles:
        
        d=float(i.split("_")[1])
        #print("(importU says) d_rel:" + str(d/maxnorm))
        if abs(d/maxnorm)>ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["w_c"]*0.5/r and abs(d/maxnorm)<1:
            #print("true")
            arr[d/maxnorm]=np.transpose(np.loadtxt(path.join(samplePath,str(dirToCheck),i), dtype=float, delimiter=',',skiprows=1, usecols=[0, 1,2,3])) 
    return arr


def interpolateUtau(x,caseName, plott=False):

    """
        x: Position wo utau bestimmt werden soll
        Punkte bestimmen, an denen y+ kleiner als 1 und knapp größer als 1 ist; Kühlgeschwindigkeit linear interpolieren
    """

    # Geschwindigkeit einlesen, kinematische Viskosität bestimmen
    
    arr = importU(caseName)
    nu=toolbox.calcNu(caseName)
    
    # Hoehenkoordinate und Hoehe bestimmen
    
    y=arr[x][0,:]
    h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]
    u=np.transpose(arr[x][1,:])
    
    # y normieren
    
    yNorm=y*u/nu
    
    yNorm0=yNorm[yNorm<1][-1]
    yNorm1=yNorm[yNorm>1][0]
    
    u0=u[yNorm==yNorm0]
    u1=u[yNorm==yNorm1] 
    
    utau=u0+(1-yNorm0)*(u1-u0)/(yNorm1-yNorm0)
    if plott:
        plt.plot(u,yNorm,'.')
        plt.axhline(1,linestyle='dashed')
       
        plt.axvline(utau,linestyle='dashed')
        plt.xlabel(r'u [$m\cdot s^{-1}$]')
        plt.ylabel(r'$y\cdot\frac{u}{nu}$ [1]')
        plt.title(r'Geschwindigkeitsprofil an der Stelle $x_{rel}=$'+str(1e-6*x/h))
        plt.yscale('linear')
        plt.grid()

         #tikzplotlib.save(path.join("./Bilder",caseName,'plt_'+str(np.ceil(1e-4*x/h)*1e-2)+caseName.split("/")[-2]+'.tex'))     
        plt.savefig("plotInterpolation.png")
        np.savetxt('Data/uTauInterpolation.csv', np.transpose(np.array([yNorm,u])), delimiter=", ", header='yNorm, u', comments='')
        file=open("Data/uTauInterpolation.txt","w")
        file.write("linear Model y=ax+b\na:    "+str((u1-u0)/(yNorm1-yNorm0))+"\n  b:  "+str(u0-yNorm0*(u1-u0)/(yNorm1-yNorm0))+"\n utau:   "+str(utau[0]))
        file.close()
 
        #plt.show()
    return utau[0]
    
    
def interpolateOverHeater(x_int,caseName,plott=False):

    # Geschwindigkeit importieren, Hoehe  und Radius auslesen, konstanten und positionen der Messstellen bestimmen

    arr = importU(caseName)
    xrel=np.abs(np.sort(np.array(list(arr.keys()))))
    h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]
    r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["radius"]
    nu=toolbox.calcNu(caseName)
    
    # Schubspannungsgeschwindigkeit fuer jede Messstelle ermitteln
    
    utau=np.array([interpolateUtau(i,caseName,False) for i in np.sort(np.array(list(arr.keys())))])
    
    # Polynom 2. Grades definieren und einen Curve fit ausfuehren
    
    def f(x,t,y): 
        return x[2]*t**2+x[1]*t+x[0]-y
    #popt, pcov = curve_fit(f, xrel, utau, method='lm', sigma=[1e-3,1e-4])
    x0 = np.ones(3)
    res_robust = least_squares(f, x0,loss='soft_l1', f_scale=0.1,  args=(xrel, utau))
    #print(res_robust)
    utauint=f(res_robust.x,np.array(x_int),np.zeros(1))
    
    
    
    #print(utauint)
    if plott:
        x2Plot=np.linspace(1,0,100)
        fig, ax=plt.subplots(1,1)
        ax.scatter(xrel,utau)
        ax.plot(x2Plot,f(res_robust.x,x2Plot,np.zeros(100)))
        ax.axvline(x_int,linestyle='dashed')
        ax.set_xlabel(r'$x$ [m]')
        ax.set_ylabel(r'$u_\tau$ [1]')
        ax.set_title(r'$u_\tau$ an der Stelle $x=$'+str(x_int))
        ax.set_yscale('linear')
        ax.grid()
        ax.invert_xaxis()
    #   tikzplotlib.save(path.join("./Bilder",caseName,'heaterInterpolation'+caseName.split("/")[-2]+'.tex'))
        #plt.savefig(path.join("./Bilder",caseName,'heaterInterpolation'+caseName.split("/")[-2]+'.png'))
        plt.savefig('uTauHeater.png')
        np.savetxt('Data/uTauHeater.csv', np.transpose(np.array([xrel,utau])), delimiter=", ", header='xrel, ut', comments='')
        file=open("Data/uTauHeaterPolynomial.txt","w")
        file.write("polynomial Model y=a^2x+bx+c\na:    "+str(res_robust.x[2])+"\n  b:  "+str(res_robust.x[1])+"\n  c:  "+str(res_robust.x[0]))
        #+"\n Koeffizienten:   "+str(pcov)+"\n Standardabweichung: "+str(np.sqrt(np.diag(pcov))))
        file.close()
    return f(res_robust.x,0,0)

def plotUProfileAlongAxis(caseName):
    # using genfromtxt()

    arr = importU(caseName)
    x=arr.keys()
    fig, ax = plt.subplots(constrained_layout=True)
    h=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["hoehe"]
    utau=ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["utau"]
    r=0.5*ParsedParameterFile(path.join(caseName,"constant","domainDimensions"))["radius"]
    nu=toolbox.calcNu(caseName)
    ystars=[]
    xstars=[]
    for i in x:
#       print(i)
        u=np.linalg.norm(np.transpose(np.array(arr[i])[1:3,:]),axis=1)
        y=np.transpose(np.array(arr[i])[0,:])
        yp=y*u/nu
        ystar=y[yp==yp[np.abs(yp-5)==min(np.abs(yp-5))]]
        ystars.append(min(ystar)/h)
        xstars.append(-0.1*min(u[y==ystar])+1e-6*i/r)
        ax.plot(-0.1*(u)-1e-6*i/r,np.transpose(np.array(arr[i])[0,:])/h)
    #ax.plot(np.array(xstars),np.array(ystars))
    ax.set_xlabel('relative Position des Geschwindigkeitsprofils [1]')
    ax.set_ylabel('relative Höhe y [1]')
    ax.set_title('Geschwindigkeitsprofile in äquidistanten Entfernungen in Strömungsrichtung')
    ax.set_xticks(np.arange(0, 1, step=0.25))
    #secax = ax.twiny()
    ax.grid()
    #ax.set_yscale('log')
    ax.legend()
    ax.invert_xaxis()
    #tikzplotlib.save(path.join("./Bilder",caseName,'uPltAlongAxis'+caseName.split("/")[-2]+'.tex'))
    plt.savefig(path.join("./Bilder",caseName,'uPltAlongAxis'+caseName.split("/")[-2]+'.png'))
    #plt.show()

