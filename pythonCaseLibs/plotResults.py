#!/usr/bin/python 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import os
import time as Time
import sys
import threading
import pickle
from . import toolbox
font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 8}

rc('font', **font)
def plotResults(caseName,fig=None,ax=None,a=None):
		res_Uz, res_Ux,res_Uy,res_prgh,res_h=toolbox.parseResiduals(caseName)
		dT=toolbox.parseTemp(caseName)
		tau=toolbox.parseTau(caseName)
		if any([fig==None,ax==None,a==None]):
			fig,ax = plt.subplots(3,1)
			a={'res_Uz':ax[0].plot(np.array(res_Uz),label='Residuum $U_z$'),
				'res_Uy':ax[0].plot(np.array(res_Uy),label='Residuum $U_y$'),
				'res_Ux':ax[0].plot(np.array(res_Ux),label='Residuum $U_x$'),
				'res_prgh':ax[0].plot(np.array(res_prgh),label=r'Residuum $\rho\cdot g\cdot h$'),
				'res_h':ax[0].plot(np.array(res_h),label='Residuum h'),
				'dT':ax[1].plot(np.array(dT),label='$\Delta T$'),
				'tau':ax[2].plot(np.array(tau),label='$\sqrt{ \u03C4 _ w}$')}
			fig.canvas.draw()
			fig.canvas.flush_events()
			ax[0].legend()
			ax[0].set_xlabel('Iterationen')
			ax[0].set_ylabel('Residuen')
			ax[2].legend()
			ax[2].set_xlabel('Iterationen')
			ax[2].set_ylabel('$\sqrt{\u03C4}$')
			ax[1].legend()
			ax[1].set_xlabel('Iterationen')
			ax[1].set_ylabel('$\Delta T$ [K]')
			ax[0].set_title('Verlauf der Residuen')
			ax[0].grid()
			ax[1].grid()
			ax[1].set_title('Temperaturunterschied zwischen vorderem und hinterem Messdraht')
			ax[2].grid()
			ax[2].set_title('Wurzel der Wandschubspannung am Heizdraht ~ $u_\u03C4$')

			ax[0].set_yscale('log')
	#	else:
	#		for i in a.keys():
	#			a[i][0].set_data(np.array(range(len(np.array(res_Uz)))),np.array(res_Uz))
	#			a[i][0].set_data(np.array(range(len(np.array(res_Ux)))),np.array(res_Ux))
	#			a[i][0].set_data(np.array(range(len(np.array(res_Uy)))),np.array(res_Uy))
	#			a[i][0].set_data(np.array(range(len(np.array(res_prgh)))),np.array(res_prgh))
	#			a[i][0].set_data(np.array(range(len(np.array(res_h)))),np.array(res_h))
	#			a[i][0].set_data(np.array(range(len(np.array(dT)))),np.array(dT))
				
						
		ax[0].relim()
		ax[0].autoscale_view()
		ax[1].relim()
		ax[1].autoscale_view()
		ax[2].relim()
		ax[2].autoscale_view()
		fig.tight_layout()
		fig.canvas.draw()
		fig.canvas.flush_events()
		return fig,ax,a
