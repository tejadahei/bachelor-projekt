from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from . import makeMesh, monitors, grenzschichtprofil, toolbox
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
from os import path

def consecutiveRun(arr, params, parallel=False, tol=1e-3, N=4):
    """
    Perform consecutive runs for a range of cases.

    Args:
        arr (iterable): Range of cases.
        params (dict): Parameters for case generation.
        parallel (bool): Flag to enable parallel execution.
        tol (float): Convergence tolerance.
        N (int): Number of processors for parallel execution.
    """
    for i in arr:
        currentCase = makeMesh.genCase(params, i, alpha=0, utau=1.0)

        if parallel:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            makeMesh.bakeMesh(currentCase, True, np=N)
            x.start()
            result = SteadyRunner(args=["--progress", "--autosense-parallel", theSolver, "-case", currentCase]).getData()
            x.join()
        else:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            makeMesh.bakeMesh(currentCase, False)
            x.start()
            result = SteadyRunner(args=["--progress", theSolver, "-case", currentCase]).getData()
            x.join()

def rerun(arr, parallel=False, tol=1e-3, N=4):
    """
    Rerun simulation cases with updated settings.

    Args:
        arr (iterable): Range of cases.
        parallel (bool): Flag to enable parallel execution.
        tol (float): Convergence tolerance.
        N (int): Number of processors for parallel execution.
    """
    
    print("rerunning ")
    for i in arr:
        currentCase = i
        ParsedParameterFile(path.join(currentCase, "system", "controlDict"))["startFrom"] = "latestTime"
        
        print("rerunning "+i)
        if parallel:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, True, np=N)
            result = SteadyRunner(args=["--progress", "--autosense-parallel", theSolver, "-case", currentCase]).getData()
            x.join()
        else:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, False)
            result = SteadyRunner(args=["--progress", theSolver, "-case", currentCase]).getData()
            x.join()

def newRun(arr, parallel=False, tol=1e-3, N=4):
    """
    Run new simulations starting from the initial time.

    Args:
        arr (iterable): Range of cases.
        parallel (bool): Flag to enable parallel execution.
        tol (float): Convergence tolerance.
        N (int): Number of processors for parallel execution.
    """
    for i in arr:
        currentCase = i
        ParsedParameterFile(path.join(currentCase, "system", "controlDict"))["startFrom"] = "startTime"

        if parallel:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, True, np=N)
            result = SteadyRunner(args=["--progress", "--autosense-parallel", theSolver, "-case", currentCase]).getData()
            x.join()
        else:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[currentCase, tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, False)
            result = SteadyRunner(args=["--progress", theSolver, "-case", currentCase]).getData()
            x.join()

def utauVariation(arr, angle, variationDirectory, parallel=False, tol=2e-4, N=4):
    """
    Perform simulations with varying shear velocities.

    Args:
        arr (iterable): Range of shear velocities.
        angle (float): Angle for case variation.
        variationDirectory (str): Directory for storing variations.
        parallel (bool): Flag to enable parallel execution.
        tol (float): Convergence tolerance.
        N (int): Number of processors for parallel execution.
    """
    for i in arr:
        currentCase = makeMesh.varyUtauMesh(i, variationDirectory, angle=angle, np=N)
        if parallel:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[path.join(variationDirectory, currentCase), tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, True, np=N)
            result = SteadyRunner(args=["--progress", "--autosense-parallel", theSolver,
                                        "-case", path.join(variationDirectory, currentCase)]).getData()
            x.join()
        else:
            x = threading.Thread(target=monitors.convergenceMonitor, args=[path.join(variationDirectory, currentCase), tol])
            print(currentCase)
            theSolver = "buoyantSimpleFoam"

            x.start()
            # makeMesh.bakeMesh(currentCase, False)
            result = SteadyRunner(args=["--progress", theSolver, "-case",
                                        path.join(variationDirectory, currentCase)]).getData()
            x.join()
