import numpy as np
import matplotlib.pyplot as plt
import math
import scipy.integrate
import csv
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from os import path



def du(y):
        
    # Integrand
        
    return 2/(1+np.sqrt(1+4*(0.4*y)**2*(1-np.exp(-y/26))**2))

def vanDriest(x):

    # kumulativ integrieren bis zur Decke

    return du(x[0])+scipy.integrate.cumtrapz(du(x), x, initial=0)

def returnVelocityProfile(yPmax, nSamples):

    #   normiertes Geschwindigkeitsprofil bis zu yPmax angeben

	ydriest=np.logspace(0,math.log10(yPmax),nSamples)
	u=vanDriest(ydriest)	
    
	return ydriest,u
	
def genRealVelocityProfile(nu,ut,h,samples,direction,caseName):
	
    #   van Driest Profil um die Schubspannungsgeschwindigkeit skalieren, yPlus in y transformieren

	dirRad=math.pi*direction/180
	yPlusMax=ut*h/nu
	yPlus,uPlusProfile=returnVelocityProfile(yPlusMax,samples)
	Profile=([np.multiply(yPlus,nu/ut),math.cos(dirRad)*np.multiply(ut,uPlusProfile),math.sin(dirRad)*np.multiply(ut,uPlusProfile), 1e-32*np.ones(np.shape(uPlusProfile))])
	#print(Profile)
	np.savetxt(path.join(caseName,"velocityProfile.csv"), np.transpose(Profile), delimiter=",")

def genSampleLines(samples,caseName):
	controlDict = ParsedParameterFile(path.join(caseName,"system","controlDict"),backup = True )
	domainDimensions = ParsedParameterFile(path.join(caseName,"constant","domainDimensions"),backup = True )
	distance=0.5*domainDimensions["radius"]
	h=domainDimensions["hoehe"]*0.125
	direction=domainDimensions["directionRad"]
	sampleRange=np.linspace(0,1,samples)
	sampleRangexCoords=-sampleRange*math.cos(direction)*distance
	sampleRangeyCoords=-sampleRange*math.sin(direction)*distance
	sampleDict=[]
	for i in range(len(sampleRange)):
		
		tempStart=[sampleRangexCoords[i],sampleRangeyCoords[i],0]
		
		tempEnd=[sampleRangexCoords[i],sampleRangeyCoords[i],h]
		tempSampleLine={"type":	"face",
															 "axis":	"z",
															 "start":	tempStart,
															 "end":		tempEnd}
		sampleDict.append(("sample_"+str(sampleRange[i]),tempSampleLine))
	controlDict["functions"]["UsampleLines"]["sets"]=sampleDict
	controlDict.writeFile()
