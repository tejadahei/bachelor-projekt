#!/usr/bin/python3.6 
from pythonCaseLibs import * 
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Applications.PlotRunner import PlotRunner
from PyFoam.Applications.SteadyRunner import SteadyRunner
import time as Time
import pickle
import threading
import json
import numpy as np
import math
import os
from os import path
with open('params.json', 'r') as fp:
    paramDict = json.load(fp)
   
utauArray=0.61
alphamin=60
alphamax=90
utauAngles=np.linspace(alphamin, alphamax,3)
#utauAngles=[-45.0, 30, 60, 90, 120, 150, 180]
#utauAngles=[75, 85, 12.5]
utauArray=0.8
#utauAngles=[0.0]
print(utauAngles)
for i in utauAngles:
	os.system('rm -r '+path.join("angleVariation",str(i),str(utauArray)))
	os.system("mkdir -p "+path.join("angleVariation",str(i),str(utauArray)))
	analyseCase.utauVariation([utauArray],i,path.join("angleVariation",str(i)),True,1e-4,N=8)
